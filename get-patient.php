<?php
$record = (object)[];
$secrets = require __DIR__ . '/secrets/secrets.php';
$username = $secrets['username'];
$password = $secrets['password'];
header('Content-Type: application/json; charset=UTF-8');
$conn = new PDO("mysql:host=localhost;dbname=$username", $username, $password);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
if(!isset($_GET['id'])) {
	$query = 'SELECT id FROM patients ORDER BY id;';
	$stmt = $conn->prepare($query);
	$stmt->execute();
	$record = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);
} else {
	$query = <<<EOD
		SELECT name, species, gender, dob FROM patients WHERE
		patients.id = ? LIMIT 1;
EOD;
	$stmt = $conn->prepare($query);
	$stmt->execute([$_GET['id']]);
	$record = $stmt->fetch(PDO::FETCH_ASSOC);
	if(!$record) {
		$record = (object)[];
	}
}
echo json_encode($record);
?>
