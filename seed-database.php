<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
?>
<html>
	<head>
		<title>Seed Database</title>
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
		<style>
			.alert-danger::before {
				content: "Error: ",
				font-weight: bold;
			}
		</style>
	</head>
	<body class="p-3">
	<h1>Seed Database</h1>
<?php
if(isset($_POST['username']) && isset($_POST['password']) && isset($_POST['number'])) {
	$fun = function($post) {
		$username = stripslashes($post['username']);
		$password = stripslashes($post['password']);
		$number = stripslashes($post['number']);
		try {
			$conn = new PDO("mysql:host=localhost;dbname=$username", $username, $password);
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		} catch(PDOException $e) {
			return '<div class="alert alert-danger" role="alert">' . $e->getMessage() . '</div>';
		}
		$create_patients_sql = <<<EOD
			CREATE TABLE `patients` (
				`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
				`species` VARCHAR(255) NOT NULL ,
				`gender` VARCHAR(255) NOT NULL ,
				`name` VARCHAR(255) NOT NULL ,
				`dob` DATE NOT NULL
			);
EOD;
		// From https://list.fandom.com/wiki/List_of_common_animals
		$species = ['Aardvark', 'Alligator', 'Alpaca', 'Anaconda', 
			'Ant', 'Antelope', 'Ape', 'Aphid', 'Armadillo', 'Asp',
			'Ass', 'Baboon', 'Badger', 'Bald Eagle', 'Barracuda',
			'Bass', 'Basset Hound', 'Bat', 'Bear', 'Beaver',
			'Bedbug', 'Bee', 'Beetle', 'Bird', 'Bison',
			'Black panther', 'Black Widow Spider', 'Blue Jay',
			'Blue Whale', 'Bobcat', 'Buffalo', 'Butterfly',
			'Buzzard', 'Camel', 'Caribou', 'Carp', 'Cat',
			'Caterpillar', 'Catfish', 'Cheetah', 'Chicken',
			'Chimpanzee', 'Chipmunk', 'Cobra', 'Cod', 'Condor',
			'Cougar', 'Cow', 'Coyote', 'Crab', 'Crane', 'Cricket',
			'Crocodile', 'Crow', 'Cuckoo', 'Deer', 'Dinosaur',
			'Dog', 'Dolphin', 'Donkey', 'Dove', 'Dragonfly', 'Duck',
			'Eagle', 'Eel', 'Elephant', 'Emu', 'Falcon', 'Ferret',
			'Finch', 'Fish', 'Flamingo', 'Flea', 'Fly', 'Fox',
			'Frog', 'Goat', 'Goose', 'Gopher', 'Gorilla',
			'Grasshopper', 'Hamster', 'Hare', 'Hawk',
			'Hippopotamus', 'Horse', 'Hummingbird',
			'Humpback Whale', 'Husky', 'Iguana', 'Impala',
			'Kangaroo', 'Ladybug', 'Leopard', 'Lion', 'Lizard',
			'Llama', 'Lobster', 'Mongoose', 'Monitor lizard',
			'Monkey', 'Moose', 'Mosquito', 'Moth', 'Mountain goat',
			'Mouse', 'Mule', 'Octopus', 'Orca', 'Ostrich', 'Otter',
			'Owl', 'Ox', 'Oyster', 'Panda', 'Parrot', 'Peacock',
			'Pelican', 'Penguin', 'Perch', 'Pheasant', 'Pig',
			'Pigeon', 'Polar bear', 'Porcupine', 'Quail', 'Rabbit',
			'Raccoon', 'Rat', 'Rattlesnake', 'Raven', 'Rooster',
			'Sea lion', 'Sheep', 'Shrew', 'Skunk', 'Snail', 'Snake',
			'Spider', 'Tiger', 'Walrus', 'Whale', 'Wolf', 'Zebra'];
		// from https://github.com/fzaninotto/Faker/blob/mastaer/src/Faker/Provider/en_GB/Person.php
    		$firstNameMale = array(
			'Aaron', 'Adam', 'Adrian', 'Aiden', 'Alan', 'Alex', 'Alexander', 'Alfie', 'Andrew', 'Andy', 'Anthony', 'Archie', 'Arthur',
			'Barry', 'Ben', 'Benjamin', 'Bradley', 'Brandon', 'Bruce',
			'Callum', 'Cameron', 'Charles', 'Charlie', 'Chris', 'Christian', 'Christopher', 'Colin', 'Connor', 'Craig',
			'Dale', 'Damien', 'Dan', 'Daniel', 'Darren', 'Dave', 'David', 'Dean', 'Dennis', 'Dominic', 'Duncan', 'Dylan',
			'Edward', 'Elliot', 'Elliott', 'Ethan',
			'Finley', 'Frank', 'Fred', 'Freddie',
			'Gary', 'Gavin', 'George', 'Gordon', 'Graham', 'Grant', 'Greg',
			'Harley', 'Harrison', 'Harry', 'Harvey', 'Henry',
			'Ian', 'Isaac',
			'Jack', 'Jackson', 'Jacob', 'Jake', 'James', 'Jamie', 'Jason', 'Jayden', 'Jeremy', 'Jim', 'Joe', 'Joel', 'John', 'Jonathan', 'Jordan', 'Joseph', 'Joshua',
			'Karl', 'Keith', 'Ken', 'Kevin', 'Kieran', 'Kyle',
			'Lee', 'Leo', 'Lewis', 'Liam', 'Logan', 'Louis', 'Lucas', 'Luke',
			'Mark', 'Martin', 'Mason', 'Matthew', 'Max', 'Michael', 'Mike', 'Mohammed', 'Muhammad',
			'Nathan', 'Neil', 'Nick', 'Noah',
			'Oliver', 'Oscar', 'Owen',
			'Patrick', 'Paul', 'Pete', 'Peter', 'Philip',
			'Quentin',
			'Ray', 'Reece', 'Riley', 'Rob', 'Ross', 'Ryan',
			'Samuel', 'Scott', 'Sean', 'Sebastian', 'Stefan', 'Stephen', 'Steve',
			'Theo', 'Thomas', 'Tim', 'Toby', 'Tom', 'Tony', 'Tyler',
			'Wayne', 'Will', 'William',
			'Zachary', 'Zach'
		);
		$firstNameFemale = array(
			'Abbie', 'Abigail', 'Adele', 'Alexa', 'Alexandra', 'Alice', 'Alison', 'Amanda', 'Amber', 'Amelia', 'Amy', 'Anna', 'Ashley', 'Ava',
			'Beth', 'Bethany', 'Becky',
			'Caitlin', 'Candice', 'Carlie', 'Carmen', 'Carole', 'Caroline', 'Carrie', 'Charlotte', 'Chelsea', 'Chloe', 'Claire', 'Courtney',
			'Daisy', 'Danielle', 'Donna',
			'Eden', 'Eileen', 'Eleanor', 'Elizabeth', 'Ella', 'Ellie', 'Elsie', 'Emily', 'Emma', 'Erin', 'Eva', 'Evelyn', 'Evie',
			'Faye', 'Fiona', 'Florence', 'Francesca', 'Freya',
			'Georgia', 'Grace',
			'Hannah', 'Heather', 'Helen', 'Helena', 'Hollie', 'Holly',
			'Imogen', 'Isabel', 'Isabella', 'Isabelle', 'Isla', 'Isobel',
			'Jade', 'Jane', 'Jasmine', 'Jennifer', 'Jessica', 'Joanne', 'Jodie', 'Julia', 'Julie', 'Justine',
			'Karen', 'Karlie', 'Katie', 'Keeley', 'Kelly', 'Kimberly', 'Kirsten', 'Kirsty',
			'Laura', 'Lauren', 'Layla', 'Leah', 'Leanne', 'Lexi', 'Lilly', 'Lily', 'Linda', 'Lindsay', 'Lisa', 'Lizzie', 'Lola', 'Lucy',
			'Maisie', 'Mandy', 'Maria', 'Mary', 'Matilda', 'Megan', 'Melissa', 'Mia', 'Millie', 'Molly',
			'Naomi', 'Natalie', 'Natasha', 'Nicole', 'Nikki',
			'Olivia',
			'Patricia', 'Paula', 'Pauline', 'Phoebe', 'Poppy',
			'Rachel', 'Rebecca', 'Rosie', 'Rowena', 'Roxanne', 'Ruby', 'Ruth',
			'Sabrina', 'Sally', 'Samantha', 'Sarah', 'Sasha', 'Scarlett', 'Selina', 'Shannon', 'Sienna', 'Sofia', 'Sonia', 'Sophia', 'Sophie', 'Stacey', 'Stephanie','Suzanne', 'Summer',
			'Tanya', 'Tara', 'Teagan', 'Theresa', 'Tiffany', 'Tina', 'Tracy',
			'Vanessa', 'Vicky', 'Victoria',
			'Wendy',
			'Yasmine', 'Yvette', 'Yvonne',
			'Zoe',
		);
		$lastName = array(
			'Adams', 'Allen', 'Anderson',
			'Bailey', 'Baker', 'Bell', 'Bennett', 'Brown', 'Butler',
			'Campbell', 'Carter', 'Chapman', 'Clark', 'Clarke', 'Collins', 'Cook', 'Cooper', 'Cox',
			'Davies', 'Davis',
			'Edwards', 'Ellis', 'Evans',
			'Fox',
			'Graham', 'Gray', 'Green', 'Griffiths',
			'Hall', 'Harris', 'Harrison', 'Hill', 'Holmes', 'Hughes', 'Hunt', 'Hunter',
			'Jackson', 'James', 'Johnson', 'Jones',
			'Kelly', 'Kennedy', 'Khan', 'King', 'Knight',
			'Lee', 'Lewis', 'Lloyd',
			'Marshall', 'Martin', 'Mason', 'Matthews', 'Miller', 'Mitchell', 'Moore', 'Morgan', 'Morris', 'Murphy', 'Murray',
			'Owen',
			'Palmer', 'Parker', 'Patel', 'Phillips', 'Powell', 'Price',
			'Reid', 'Reynolds', 'Richards', 'Richardson', 'Roberts', 'Robertson', 'Robinson', 'Rogers', 'Rose', 'Ross', 'Russell',
			'Saunders', 'Scott', 'Shaw', 'Simpson', 'Smith', 'Stevens', 'Stewart',
			'Taylor', 'Thomas', 'Thompson', 'Turner',
			'Walker', 'Walsh', 'Ward', 'Watson', 'White', 'Wilkinson', 'Williams', 'Wilson', 'Wood', 'Wright',
			'Young',
		);
		try {
			$conn->exec($create_patients_sql);
		} catch(PDOException $e) {
			return '<div class="alert alert-danger" role="alert">' . $e->getMessage() . '</div>';
		}
		try {
			if($number > 1000) {
				$number = 1000;
			}
			$query = "INSERT INTO patients (species, gender, name, dob) VALUES " . implode(', ', array_fill(0, $number, '(?, ?, ?, ?)')) . ';';
			$stmt = $conn->prepare($query);
			$now = time();
			$hundred_years = 3153600000;
			$data = [];
			for($i = 0;$i < $number;$i++) {
				// according to
				// https://www.ipsos.com/sites/default/files/LGBT%20Pride%202021%20Global%20Survey%20Report%20-%20US%20Version.pdf
				// 1% of people identify as non-binary, perhaps
				// this would be true for animals as well...
				$gender_int = rand(1, 101);
				$gender = null;
				$first_name_arr = null;
				if($gender_int === 1) {
					$gender = 'Non-binary';
					$first_name_arr = rand(0, 1) ? $firstNameMale : $firstNameFemale;
				} else if($gender_int > 50) {
					$gender = 'Female';
					$first_name_arr = $firstNameFemale;
				} else {
					$gender = 'Male';
					$first_name_arr = $firstNameMale;
				}
				$first_name = $first_name_arr[array_rand($first_name_arr)];
				$last_name = $lastName[array_rand($lastName)];
				$name = $first_name . ' ' . $last_name;
				$specie = $species[array_rand($species)];
				// normal distribution of age
				$nrand = function($mean, $sd) use(&$nrand) {
					$x = mt_rand()/mt_getrandmax();
					$y = mt_rand()/mt_getrandmax();
					$r = sqrt(-2*log($x))*cos(2*pi()*$y)*$sd + $mean;
					if($r > 0) {
						return $r;
					}
					return $nrand($mean, $sd);
				};
				// seconds in a year
				$seconds_old = $nrand(40, 15) * 31556952;
				$dob_timestamp = $now - $seconds_old;
				array_push($data,
					$specie,
					$gender,
					$name,
					date('Y-m-d H:i:s', $dob_timestamp)
				);
			}
			$stmt->execute($data);
		} catch(PDOException $e) {
			return '<div class="alert alert-danger" role="alert">' . $e->getMessage() . '</div>';
		}
		return '<div class="alert alert-success" role="alert">Database seeded successfully</div>';
	};
	echo $fun($_POST);
} else {
	echo <<<EOD
	<form action="seed-database.php" method="POST" style="max-width:20em;"
		<div class="mb-3">
			<label for="username" class="form-label">MySQL Username</label>
			<input type="text" class="form-control" id="username" name="username">
		</div>
		<div class="mb-3">
			<label for="password" class="form-label">Password</label>
			<input type="password" class="form-control" id="password" name="password">
		</div>
		<div class="mb-3">
			<label for="number" class="form-label">Number of records</label>
			<input type="range" min="1" max="1000" value="100" class="slider" name="number" id="number" onInput="document.getElementById('rangeOutput').innerHTML = this.value;">
			<small id="rangeOutput" class="form-text text-muted"></small>
		</div>
		<button type="submit" class="btn btn-primary">Submit</button>
	</form>	
EOD;
}
?>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
		<script>
			document.addEventListener('DOMContentLoaded', function() {
				document.getElementById('rangeOutput').innerHTML = document.getElementById('number').value;
			});
		</script>
	</body>
</html>
